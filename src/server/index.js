
// //dependencies for server
import express from 'express'
import graphqlHTTP from 'express-graphql'
import session from 'express-session';
import cors from 'cors'
import schema from '../graph/appSchema'
import passport from '../graph/passport'

let app = express()

const PORT = process.env.PORT
const HOST = process.env.HOST

class Server {

	init() {

		app.use(express.json())
		app.use(express.urlencoded({
			extended: true
		}))
		app.use(cors({
			credentials: true,
			origin: "https://vegner.tk"
		}))

		app.use(
      session({
				secret: 'shhh',
				saveUninitialized: true,
				resave: true,
				cookie: { maxAge: 6000 }
      })
		);

		passport.initialize()
		app.use('/api', (req, res, next) => {
			passport.authenticate('jwt', {session: false}, (err, user, info) => {
				if (err) next()
				if (info) next ()

				if (user) req.user = user

				next()
			})(req, res, next)
		})

    app.use('/api' ,graphqlHTTP((req, res) => ({
			schema: schema,
			rootValue: { session: req.session },
			graphiql: true,
			context: {req, res}
		})));
		
		app.use('*', (req, res) => {
			res.set('Content-Type', 'javascript/json');
		})

    app.listen('vegner.tk', 3000, () => {
      console.log(`server listening ${HOST}:${PORT}`);
    });
	}
}

export default new Server()
