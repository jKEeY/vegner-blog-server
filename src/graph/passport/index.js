import passport from 'passport';

const JwtStrategy = require('passport-jwt').Strategy
const ExtractJwt = require('passport-jwt').ExtractJwt

import userModel from '../../db/models/userSchema'

let opts = {}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken()
opts.secretOrKey = 'shhh'
opts.issuer = 'http://localhost:3000/api'
opts.audience = 'http://localhost:8080'

passport.use(new JwtStrategy(opts, async (payload, done) => {
  try {
    const user = await userModel.findById(payload.sub)
    if (!user) return done(null, false)
  
    done(null, true)  
  } catch (err) {
    done(null, false)
  }

}))

export default passport