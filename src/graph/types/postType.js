import {
	GraphQLString,
	GraphQLObjectType,
	GraphQLNonNull,
	GraphQLBoolean,
	GraphQLList,
	GraphQLInt
} from 'graphql'

import userType from './userType'
import commentType from './commentType'
import likeType from './likeType'
import disLikeType from './disLikeType'

import userSchema from '../../db/models/userSchema'
import commentSchema from '../../db/models/commentSchema'
import likeSchema from '../../db/models/likeSchema';
import disLikeSchema from '../../db/models/disLikeSchema';

export default new GraphQLObjectType({
	name: 'Post',
	description: 'This todo list type',
	fields: () => ({
		_id: {
			type: new GraphQLNonNull(GraphQLString),
			resolve: (post) => {
				return post._id.toString()
			}
		},
		title: {type: new GraphQLNonNull(GraphQLString)},
		description: {type: new GraphQLNonNull(GraphQLString)},
		body: {type: new GraphQLNonNull(GraphQLString)},
		created: {type: GraphQLString, resolve: (post) => {
			return post.created.toString()
		}},
		author: {type: userType, resolve: (post) => {
			 return userSchema.findOne({_id: post.author_id}).then(data => data).catch(err => err)
		}},
		isOpened: {type: GraphQLBoolean, resolve: () => {
			return false
		}},
		comments: {type: GraphQLList(commentType), resolve: (post) => {

			return commentSchema.find({post_id: post._id}).then(data => data)
		}},
		isLiked: {
      type: GraphQLBoolean,
      resolve: (post) => {
				return likeSchema.findOne({post_id: post._id, author_id: post.sub}).then(data => {
					if (data) return true
					else return false
				})
			}
      
		},
		isDisLiked: {
      type: GraphQLBoolean, 
      resolve: (post) => {
        return disLikeSchema.findOne({post_id: post._id, author_id: post.sub}).then(data => {
					if (data) return true
					else return false
				})
      }
		},
    likes: {
      type: GraphQLInt,
      resolve: (post) => {
        return likeSchema.find({post_id: post._id}).then(data => data.length)
      }
    },
    disliked: {
      type: GraphQLInt, 
      resolve: (post) => {
        return disLikeSchema.find({post_id: post._id}).then(data => data.length)
      }
    }
	})
})