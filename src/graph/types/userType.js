import {
	GraphQLString,
	GraphQLObjectType,
	GraphQLNonNull
} from 'graphql'

export default new GraphQLObjectType({
	name: 'User',
	description: 'This user type',
	fields: () => ({
		_id: {
			type: new GraphQLNonNull(GraphQLString),
			resolve: (user) => {
				
				return user._id.toString()
			}
		},
		username: {type: new GraphQLNonNull(GraphQLString)}
	})
})