import {
	GraphQLObjectType,
	GraphQLBoolean
} from 'graphql'
import postSchema from '../../db/models/postSchema'

export default new GraphQLObjectType({
	name: 'createPost',
	description: 'This create post type',
	fields: () => ({
		success: {type: GraphQLBoolean, resolve: ({title, description, body, author_id}) => {
			const post = new postSchema ({
				title,
				description,
				body,
				author_id
			})
			 return post.save().then(data => true).catch(err => false)
		}}
	})
})