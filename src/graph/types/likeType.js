import {
	GraphQLObjectType,
  GraphQLBoolean,
  GraphQLInt
} from 'graphql'

export default new GraphQLObjectType({
	name: 'Like',
	description: 'This like type',
	fields: () => ({
		isLiked: {
      type: GraphQLBoolean,
      resolve: () => {
        return true
      }
		},
    likes: {
      type: GraphQLInt,
      resolve: () => {
        return 15
      }
    }
	})
})