import {
	GraphQLObjectType,
  GraphQLBoolean,
  GraphQLInt
} from 'graphql'

export default new GraphQLObjectType({
	name: 'disLike',
	description: 'This dislike type',
	fields: () => ({
		isDisLiked: {
      type: GraphQLBoolean, 
      resolve: () => {
        return false
      }
		},
    disliked: {
      type: GraphQLInt, 
      resolve: () => {
        return 20
      }
    }
	})
})