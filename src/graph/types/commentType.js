import {
	GraphQLString,
	GraphQLObjectType,
	GraphQLNonNull
} from 'graphql'
import userSchema from '../../db/models/userSchema'

export default new GraphQLObjectType({
	name: 'Comment',
	description: 'This comment type',
	fields: () => ({
    post_id: {type: new GraphQLNonNull(GraphQLString)},
    author_id: {type: new GraphQLNonNull(GraphQLString)},
    created: {type: GraphQLString, resolve: (comment) => {
			return comment.created.toString()
		}},
    username: {type: GraphQLString, resolve: (comment) => {
      return userSchema.findOne({_id: comment.author_id}).then(data => data.username).catch(err => err)
    }},
    message: {type: new GraphQLNonNull(GraphQLString)}
	})
})