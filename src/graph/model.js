import userModel from '../db/models/userSchema'
import postModel from '../db/models/postSchema'
import commentModel from '../db/models/commentSchema'
import likeModel from '../db/models/likeSchema'
import disLikeModel from '../db/models/disLikeSchema'
import jwt from 'jsonwebtoken'
import bcrypt from 'bcrypt'

const authUser = async (decoded) => {
  return new Promise((resolve, reject) => {
    userModel.findById(decoded.sub, (err, user) => {
      if (err) reject(err)
      if(!user) reject(null)
      resolve(user)
    })
  })
}    

const getUsername = async (username) => {
  return new Promise((resolve, reject) => {
    userModel.findOne({username: username}, (err, user) => {
      if (err) reject(err)
      if (!user) reject(null)

      resolve(user)
    })
  })
}

const signToken = user => {
  return jwt.sign({
    iss: 'localhost:8080',
    sub: user._id,
    iat: new Date().getTime(),
    exp: new Date().setDate(new Date().getDate() + 1)
  }, 'shhh')
}

class ModelIndex {
  async login(root, params, {req, res}, ast) {
    try {
      const getUser = await getUsername(params.username).then(user => user).catch(err => err)
      if (getUser !== null) {
        const answer = await bcrypt.compare(params.password, getUser.password).then(res => {
          if (res) return true
          else return false
        })
        if (answer)  {
          const token = signToken(getUser)
  
          res.cookie("token", token, {
            httpOnly: false,
            maxAge: 1000 * 60 * 60 * 24 * 7 // 7 days
          })
          return {success: true, message: 'Success'}
        } else {
          return {success: false, message: 'No User, failed email or password'}
        }
      }
    } catch (e) {
      console.log(e)
      return {success: false, message: 'Erron in the server, Sory :('}
    }
  }
  async register(root, params, {req, res}, ast) {

    const user = new userModel({
      username: params.username,
      password: params.password
    })

    const token = signToken(user)

    res.cookie("token", token, {
      httpOnly: false,
      maxAge: 1000 * 60 * 60 * 24 * 7 // 7 days
    })

    await user.save()

    return {success: true, message: 'Success'}
  }

  async check(root, params, {req, res}, ast) {
    let authToken = req.headers['authorization']
    if (typeof authToken === 'undefined') {
      return false
    } else {
      const token = authToken.slice(7)
      const ver = await jwt.verify(token, 'shhh', function(err, decoded) {
        if (err) return false
        if (decoded === 'undefined') return false
        return true
      })
      return ver
    }
    
  }
  async profile(root, params, {req, res}, ast) {
    try {
      let authToken = req.headers['authorization']
      const token = authToken.slice(7)
      const ver = jwt.verify(token, 'shhh', function(err, decoded) {
        if (err) return {message: 'err', success: false}
        if (decoded === 'undefined') return {message: 'decoded', success: false}
        return decoded
      })

      const user = await authUser(ver).then(user => user)
      return user
    } catch (e) {
      return null
    }
    
  }

  async add(root, params, {req, res}, ast) {
    try {
      let authToken = req.headers['authorization']
      const token = authToken.slice(7)
      const ver = jwt.verify(token, 'shhh', function(err, decoded) {
        if (err) return {success: false}
        if (decoded === 'undefined') return {success: false}
        return {success: true}
      })
      if (!ver) return {success: false}
      else {
        const post = new postModel({
          title: params.title,
          body: params.body,
          description: params.description,
          author_id: params.author_id
        })

        post.save()

        return {success: true}
      }
    } catch (e) {
      console.log(e)
      return {success: false}
    }
  }

  async comment(root, params, {req, res}, ast) {
    try {
      let authToken = req.headers['authorization']
      const token = authToken.slice(7)
      const ver = jwt.verify(token, 'shhh', function(err, decoded) {
        if (err) return {success: false}
        if (decoded === 'undefined') return {success: false}
        return {success: true}
      })
      if (!ver) return {success: false}
      else {
        const comment = new commentModel({
          post_id: params.post_id,
          author_id: params.author_id,
          message: params.message
        })

        comment.save()

        return {success: true}
      }
    } catch (e) {
      console.log(e)
      return {success: false}
    }
  }

  async addLike (root, params, {req, res}, ast) {
    try {
      let authToken = req.headers['authorization']
      const token = authToken.slice(7)
      const ver = jwt.verify(token, 'shhh', function(err, decoded) {
        if (err) return false
        if (decoded === 'undefined') return false
        return true
      })
      if (!ver) return {success: false}
      else {
        const isLike = await likeModel.findOne({
          post_id: params.post_id,
          author_id: params.author_id,
        }).then(data => {
          if (data) return true
          else return false
        })

        const isDisLike =  await disLikeModel.findOne({
          post_id: params.post_id,
          author_id: params.author_id,
        }).then(data => {
          if (data) return true
          else return false
        })

        if (!isLike && !isDisLike) {
          const like = new likeModel({
            post_id: params.post_id,
            author_id: params.author_id,
          })
  
          like.save()
  
          return {success: true}
        } else return {success: false}
      }
    } catch (e) {
      console.log(e)
      return {success: false}
    }
  }

  async deleteLike (root, params, {req, res}, ast) {
    try {
      let authToken = req.headers['authorization']
      const token = authToken.slice(7)
      const ver = jwt.verify(token, 'shhh', function(err, decoded) {
        if (err) return false
        if (decoded === 'undefined') return false
        return true
      })
      if (!ver) return {success: false}
      else {
        const isDeleted = await likeModel.findOneAndDelete({post_id: params.post_id, author_id: params.author_id}).then((data => {
          if (data) return true
          else return false
        }))
        if (isDeleted) return {success: true}
        else return {success: false}
      }
    } catch (e) {
      console.log(e)
      return {success: false}
    }
  }

  async addDisLike (root, params, {req, res}, ast) {
    try {
      let authToken = req.headers['authorization']
      const token = authToken.slice(7)
      const ver = jwt.verify(token, 'shhh', function(err, decoded) {
        if (err) return false
        if (decoded === 'undefined') return false
        return true
      })
      if (!ver) return {success: false}
      else {
         
        const isLike = await likeModel.findOne({
          post_id: params.post_id,
          author_id: params.author_id,
        }).then(data => {
          if (data) return true
          else return false
        })
        const isDisLike =  await disLikeModel.findOne({
          post_id: params.post_id,
          author_id: params.author_id,
        }).then(data => {
          if (data) return true
          else return false
        })

        if (!isLike && !isDisLike) {
          const dislike = new disLikeModel({
            post_id: params.post_id,
            author_id: params.author_id,
          })
  
          dislike.save()
  
          return {success: true}
        } else return {success: false}
      }
    } catch (e) {
      console.log(e)
      return {success: false}
    }
  }

  async deleteDisLike (root, params, {req, res}, ast) {
    try {
      let authToken = req.headers['authorization']
      const token = authToken.slice(7)
      const ver = jwt.verify(token, 'shhh', function(err, decoded) {
        if (err) return false
        if (decoded === 'undefined') return false
        return true
      })
      if (!ver) return {success: false}
      else {
        const isDisLiked = await disLikeModel.findOneAndDelete({post_id: params.post_id, author_id: params.author_id}).then((data => {
          if (data) return true
          else return false
        }))
        if (isDisLiked) {
          return {success: true}
        }
      }
    } catch (e) {
      console.log(e)
      return {success: false}
    }
  }

}

export default new ModelIndex()
