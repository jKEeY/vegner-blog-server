import {
	GraphQLSchema
} from 'graphql'
import blogType from './query/BlogQueryRootType'
import blogMutation from './mutation/BlogMutationRootType'

export default new GraphQLSchema({
	query: blogType,
	mutation: blogMutation
})