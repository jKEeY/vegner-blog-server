import {
	GraphQLObjectType,
  GraphQLString,
  GraphQLBoolean
} from 'graphql'
import model from '../../model'


export default {
  type: new GraphQLObjectType({
    name: 'createPost',
    fields: {
      success: { type: GraphQLBoolean }
    }
  }),
  args: {
    title: {type: GraphQLString},
    description: {type: GraphQLString},
    body: {type: GraphQLString},
    author_id: {type: GraphQLString}
  },
  async resolve(root, params, context, ast) {
    const res = await model.add(...arguments)
    
    return res
  }
}