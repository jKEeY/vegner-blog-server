import {
	GraphQLObjectType,
  GraphQLString,
  GraphQLBoolean
} from 'graphql'
import model from '../../model'


export default {
  type: new GraphQLObjectType({
    name: 'deleteDisLike',
    fields: {
      success: { type: GraphQLBoolean }
    }
  }),
  args: {
    post_id: {type: GraphQLString},
    author_id: {type: GraphQLString}
  },
  async resolve(root, params, context, ast) {
    const res = await model.deleteDisLike(...arguments)
    
    return res
  }
}