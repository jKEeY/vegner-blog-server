import {
	GraphQLObjectType,
  GraphQLString,
  GraphQLBoolean
} from 'graphql'
import model from '../../model'

export default {
  type: new GraphQLObjectType({
    name: 'SignUp',
    fields: {
      success: { type: GraphQLBoolean },
      message: { type: GraphQLString }
    }
  }),
  args: {
    username: {type: GraphQLString},
    password: {type: GraphQLString}
  },
  resolve(root, params, context, ast) {
    const res = model.register(...arguments)
    return res
  }
}