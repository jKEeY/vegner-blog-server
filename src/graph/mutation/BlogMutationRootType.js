import {
	GraphQLObjectType,
	GraphQLString,
} from 'graphql'
import signup from './type/signup'
import createPost from './type/createPost'
import comment from './type/addCommet'
import addLike from './type/addLike'
import deleteLike from './type/deleteLike'
import addDislike from './type/addDisLike'
import deleteDisLike from './type/deleteDisLike'

export default new GraphQLObjectType({
	name: 'Mutation',
	description: 'This mutations schema',
	fields: () => ({
		signup,
		createPost,
		comment,
		addLike,
		deleteLike,
		addDislike,
		deleteDisLike
	})
})