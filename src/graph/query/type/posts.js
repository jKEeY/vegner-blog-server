import postType from '../../types/postType'
import postSchema from '../../../db/models/postSchema'

import jwt from 'jsonwebtoken'

import {
	GraphQLList,
} from 'graphql'

export default {
  type: new GraphQLList(postType),
  resolve: async (root, params, {req, res}, ast) => {
    let authToken = req.headers['authorization']
    const token = authToken.slice(7)
    const ver = jwt.verify(token, 'shhh', function(err, decoded) {
      if (err) return {message: 'err', success: false}
      if (decoded === 'undefined') return {message: 'decoded', success: false}
      return decoded
    })
    const data = await postSchema.find().then(data => data).catch(err => err)
    data.forEach(post => {
      post['sub'] = ver.sub
    });
    return data
  }
}
