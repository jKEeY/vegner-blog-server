import {
	GraphQLString
} from 'graphql'
import model from '../../model'

export default {
  type: GraphQLString,
  resolve(root, params, context, ast) {
    let res = model.logout(...arguments)
    
    return res
  }
}
