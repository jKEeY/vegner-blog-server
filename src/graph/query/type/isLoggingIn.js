import {
  GraphQLBoolean,
} from 'graphql'

import model from '../../model'

export default {
  type: GraphQLBoolean,
  resolve(root, params, context, ast) {
    let res = model.check(...arguments)
    return res
  }
}