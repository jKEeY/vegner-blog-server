import {
	GraphQLObjectType,
  GraphQLBoolean,
  GraphQLString
} from 'graphql'

import model from '../../model'

export default {
  type: new GraphQLObjectType({
    name: 'login',
    fields: {success: {type: GraphQLBoolean}, message: {type: GraphQLString}}
  }),
  args: {
    username: {
      type: GraphQLString
    },
    password: {
      type: GraphQLString
    }
  },
  resolve(root, params, context, ast) {
    let res = model.login(...arguments)

    return res
  }
}