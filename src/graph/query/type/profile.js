import UserType from '../../types/userType'
import model from '../../model'

export default {
  type: UserType,
  async resolve(root, params, context, ast) {
   try {
    let res = model.profile(...arguments)
    const getRes = await res.then(user => {return user})
    
    return getRes
   } catch (e) {
     return {failed: e}
   }
  }
}