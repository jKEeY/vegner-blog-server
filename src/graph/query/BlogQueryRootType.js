import {
	GraphQLObjectType
} from 'graphql'

import posts from './type/posts'
import login from './type/login'
import logout from './type/logout'
import isLoggingIn from './type/isLoggingIn'
import profile from './type/profile'

export default new GraphQLObjectType({
	name: 'Query',
	description: 'This query schema',
	fields: () => ({
		posts,
		login,
		isLoggingIn,
		logout,
		profile
	})
})