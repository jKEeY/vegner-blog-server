import mongoose from 'mongoose'

const postSchema = mongoose.Schema({
	title: {
		type: String,
		required: true
	},
	body: {
		type: String,
		required: true
	},
	description: {
		type: String,
		required: false
	},
	created: {
		type: Date,
		default: Date.now()
	},
	author_id: {
		type: String,
		required: true
	},
	comments: {
		type: Array,
		default: null
	}
})

export default mongoose.model('Post', postSchema)