	import mongoose from 'mongoose'
import bcrypt from 'bcrypt'

const userSchema = mongoose.Schema({
	username: {
		type: String,
		required: true,
		unique: true
	},
	password: {
		type: String,
		required: true
	}
})

userSchema.pre('save', function (next){
	let user = this


	bcrypt.genSalt(10, (err, salt) => {
		if (err) return next(err)

		bcrypt.hash(user.password, salt, (err, hash) => {
			if (err) return next(err)

			user.password = hash
			next()
		})
	})
})

userSchema.methods.comparePassword = (passwordHash, cb) => {
	bcrypt.compare(passwordHash, this.password, (err, isMatch) => {
		if (err) return cb(err)

		cb(null, isMatch)
	})
}

export default mongoose.model('User', userSchema)