import mongoose from 'mongoose'

const commentSchema = mongoose.Schema({
  post_id: {
    type: String,
		required: true,
  },
	author_id: {
		type: String,
		required: true
	},
	message: {
		type: String,
		required: true
  },
  created: {
    type: Date,
    default: Date.now()
  }
})

export default mongoose.model('Comment', commentSchema)