import mongoose from 'mongoose'

const LikeSchema = mongoose.Schema({
  post_id: {
    type: String,
		required: true,
  },
	author_id: {
		type: String,
		required: true
	}
})

export default mongoose.model('Like', LikeSchema)