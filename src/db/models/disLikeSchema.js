import mongoose from 'mongoose'

const disLikeSchema = mongoose.Schema({
  post_id: {
    type: String,
		required: true,
  },
	author_id: {
		type: String,
		required: true
	}
})

export default mongoose.model('disLike', disLikeSchema)