import mongoose from 'mongoose'
import { configDB } from './configDB'

export default new Promise((resolve, reject) => {

	mongoose.connect(
		configDB,
		{ 
			useNewUrlParser: true,
			useCreateIndex: true,
			useNewUrlParser: true }
	)
	const db = mongoose.connection
	db.on('error', (error) => {
		reject(error)
	})

	db.once('open', () => {
		resolve('DB connetion...')
	})

})