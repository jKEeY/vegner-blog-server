import Server from './server'
import db from './db'

db
	.then((el) => {
		console.log(el)
		Server.init()
	})
	.catch(error => {
		throw error
	})
